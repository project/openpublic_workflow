<?php
/**
 * @file
 *  App configuration for openpublic workflow app.
 */

/**
 * Implements hook_apps_app_info().
 */
function openpublic_workflow_apps_app_info() {
  return array(
    'configure form' => 'openpublic_workflow_app_configure_form',
    'status callback' => 'openpublic_workflow_status',
    'permissions' => array(
      'moderate content from archived to draft'=> array(
        'administrator' => 'administrator',
        'publisher' => 'publisher',
      ),
      'moderate content from archived to published'=> array(
        'administrator' => 'administrator',
        'publisher' => 'publisher',
      ),
      'moderate content from draft to needs_review'=> array(
        'administrator' => 'administrator',
        'editor' => 'editor',
        'publisher' => 'publisher',
        'staff' => 'staff',
      ),
      'moderate content from draft to published'=> array(
        'administrator' => 'administrator',
        'publisher' => 'publisher',
      ),
      'moderate content from needs_review to draft'=> array(
        'administrator' => 'administrator',
        'publisher' => 'publisher',
      ),
      'moderate content from needs_review to published'=> array(
        'administrator' => 'administrator',
        'publisher' => 'publisher',
      ),
      'moderate content from published to archived'=> array(
        'administrator' => 'administrator',
        'publisher' => 'publisher',
      ),
    ),
  );
}

/**
 * Provides configuration form. Needed to show hook_status below.
 */
function openpublic_workflow_app_configure_form() {
  $form = array();
  return system_settings_form($form);
}

/**
 * Implements pseudo hook_status().
 */
function openpublic_workflow_status() {
  drupal_add_library('system', 'drupal.ajax');
  $status = array(
    'title' => t('Status'),
    'headers' => array(t('severity'), t('title'), t('description'), t('action')),
    'items' => array(),
  );

  $content_types = node_type_get_types();
  ksort($content_types);

  foreach ($content_types as $content_type) {
    $options = variable_get('node_options_' . $content_type->type);
    $workflow_enabled = in_array('moderation', $options) ? TRUE : FALSE;
    $link = '';
    if (in_array('revision', $options)) {
      $link = openpublic_workflow_create_toggle_link($content_type, $workflow_enabled);
    }
    else {
      $link = l(t('Enable "Create new revision"'), 'admin/structure/types/manage/' . strtr($content_type->type, array('_' => '-')), array(
        'query' => drupal_get_destination(),
        'fragment' => 'edit-workflow',
      ));
    }
    $status['items'][$content_type->type] = array(
      'title' => $content_type->name,
      'description' => $workflow_enabled ? t("Workflow enabled for this content type.") : t("Workflow is not enabled for this content type."),
      'severity' => $workflow_enabled ? REQUIREMENT_OK : REQUIREMENT_INFO,
      'action' => array($link),
    );
  }
  $status['items']['workbench_conf'] = array(
    'title' => t("Workbench configuration"),
    'description' => t("Review Workbench states, transitions and check permissions."),
    'severity' => REQUIREMENT_INFO,
    'action' => array(l("settings", "admin/config/workbench/moderation",  array('query'  => drupal_get_destination()))),
  );
  return $status;
}
