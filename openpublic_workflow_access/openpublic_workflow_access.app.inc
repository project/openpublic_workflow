<?php

function openpublic_workflow_access_apps_app_info() {
  return array(
    'configure form' => 'openpublic_workflow_access_configure_form',
    'status callback' => 'openpublic_workflow_access_status',
    'permissions' => array(
      'access workbench access by role' => array(
        'administrator' => 'administrator',
        'authenticated user' => 'authenticated user',
      ),
      'administer workbench access' => array(
        'administrator' => 'administrator',
      ),
      'assign workbench access' => array(
        'administrator' => 'administrator',
      ),
      'batch update workbench access' => array(
        'administrator' => 'administrator',
      ),
      'view workbench access information' => array(
        'administrator' => 'administrator',
      ),
      'view workbench taxonomy pages' => array(
        'administrator' => 'administrator',
      ),
    ),
  );
}

/**
 * Provides configuration form. Needed to show hook_status below.
 */
function openpublic_workflow_access_configure_form() {
  $form = array();
  return system_settings_form($form);
}

/**
 * Implements pseudo hook_status().
 */
function openpublic_workflow_access_status() {
  // Setup the status table
  $status = array(
    'title' => t('Status'),
    'headers' => array(t('severity'), t('title'), t('description'), t('action')),
    'items' => array(),
  );

  $status['items']['workbench_access'] = array(
    'title' => 'Workbench Access',
    'action' => array(
      l(t("Administer settings"), "admin/config/workbench/access/settings",  array('query'  => drupal_get_destination())),
    ),
    'description' => t("Workbench access seems to be configured."),
    'severity' => REQUIREMENT_OK,
  );

  // Get the avaible options.
  $options = workbench_access_get_active_tree();

  if (empty($options) && !variable_get('workbench_access', FALSE)) {
    $status['items']['workbench_access']['description'] = workbench_access_configuration_needed_message();
    $status['items']['workbench_access']['severity'] = REQUIREMENT_ERROR;
  }
  else {
    $has_assignments = (bool) db_query_range('SELECT 1 FROM {workbench_access_user}', 0, 1)->fetchField()
      || (bool) db_query_range('SELECT 1 FROM {workbench_access_role}', 0, 1)->fetchField();
    $status['items']['users'] = array(
      'title' => t('Users'),
      'action' => array(l("Change users for sections", "admin/config/workbench/access",  array('query'  => drupal_get_destination()))),
      'description' => t("Administer which users can edit which sections.") . (!$has_assignments ? ' ' . t('No users have been assigned yet.') : ''),
      'severity' => $has_assignments ? REQUIREMENT_INFO : REQUIREMENT_ERROR,
    );
    $status['items']['roles'] = array(
      'title' => t('Roles'),
      'action' => array(
        l(t("Change roles for sections"), "admin/config/workbench/access/roles",  array('query'  => drupal_get_destination()))
      ),
      'description' => t("Administer which roles can edit which sections.") . (!$has_assignments ? ' ' . t('No roles have been assigned yet.') : ''),
      'severity' => $has_assignments ? REQUIREMENT_INFO : REQUIREMENT_ERROR,
    );
    $status['items']['sections'] = array(
      'title' => t('Sections'),
      'action' => array(
        l(t("Change editors for sections"), "admin/config/workbench/access/sections",  array('query'  => drupal_get_destination())),
      ),
      'description' => t("Administer your sections.") ,
      'severity' => $options ? REQUIREMENT_INFO : REQUIREMENT_ERROR,
    );
  }

  return $status;
}
